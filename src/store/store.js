import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({

  state: {
    velocidad: 0,
    aceleracion: 10,
    desacerelacion: 10,
  },
  mutations: {
    acelerar(state) {
      state.velocidad += state.aceleracion
    },
    frenar(state) {
      if(state.velocidad>0)
        state.velocidad -= state.desacerelacion
    }
  }

});